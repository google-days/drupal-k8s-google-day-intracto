FROM drupal:8.7-apache

LABEL maintainer="Niek Heezemans <niek@frontmen.nl>"

ENV NODE_ENV=docker

# Get our toolsSymfpho
RUN apt-get update && apt-get install -y \
	curl \
	git \
	mysql-client \
	nano \
	wget

# Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	mv composer.phar /usr/local/bin/composer && \
	php -r "unlink('composer-setup.php');"

# Install Drush
RUN wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar && \
	chmod +x drush.phar && \
	mv drush.phar /usr/local/bin/drush

COPY ./app /app

# Change the Workdir to the App-folder where our site lives
WORKDIR /app

# Require Prestissimo for fast install of Dependencies
RUN composer global require hirak/prestissimo

# Install all Dependencies
RUN composer install --no-interaction

# Remove all from www/html folder, we don't need it
RUN rm -rf /var/www/html/*

# Remove any config files and override it with our own
RUN rm -rf /etc/apache2/sites-enabled/*
COPY ./custom-settings/000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN find /app/private_files -type f -exec chmod 755 {} \;
RUN find /app/web/sites/default/ -type f -exec chmod -R 755 {} \;
RUN find /app/web/sites/default/settings.php -type f -exec chmod 644 {} \;
RUN find /app/web/sites/default/files/ -type f -exec chmod -R 755 {} \;

EXPOSE 80

# CMD drush cim -y