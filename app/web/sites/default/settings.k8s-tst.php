<?php
/* DEVELOPMENT */

/**
 * Database Settings
 */
$databases['default']['default'] = array (
  'database' => $_ENV['MYSQL_DATABASE'],
  'username' => $_ENV['MYSQL_USER'],
  'password' => $_ENV['MYSQL_PASSWORD'],
  'prefix' => '',
  'host' => $_ENV['MYSQL_HOST'],
  'port' => $_ENV['MYSQL_PORT'],
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
// Set the Site Name
$config['system.site']['name'] = 'Drupal';

//Set the Temporary Download Folder
$config['system.file']['path']['temporary'] = '/tmp/';

// Private Files Folder
$settings['file_private_path'] = '../private_files';

// Trusted Host Settings
$settings['trusted_host_patterns'] = array(
  '^cms.frontmen.dev$'
);